package com.zenika.poei.evaluation.domain;

import java.util.ArrayList;
import java.util.List;

public class Quiz {

    private final List<Question> questions = new ArrayList<>();

    public Quiz(List<Question> questions) {
        indexQuestions(questions);
    }

    private void indexQuestions(List<Question> questions) {
        for (int i = 0; i < questions.size(); i++) {
            int displayedIndex = i + 1;
            this.questions.add(Question.indexedQuestion(displayedIndex, questions.get(i)));
        }
    }

    public QuizGame newGame() {
        return new QuizGame(questions);
    }
}
