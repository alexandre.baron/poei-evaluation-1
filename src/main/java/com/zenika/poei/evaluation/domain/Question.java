package com.zenika.poei.evaluation.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Question {

    public static Question question(String label, List<Answer> answers, Set<Answer> correctAnswers, String explanation) {
        return new Question(null, label, answers, correctAnswers, explanation);
    }

    public static Question indexedQuestion(int index, Question question) {
        return new Question(index, question.label, question.answers, question.correctAnswers, question.explanation);
    }

    /**
     * Index of question in the Quiz. Is present only if the question is in a Quiz.
     */
    private final Integer index;

    private final String label;

    private final List<Answer> answers;

    private final Set<Answer> correctAnswers;

    private final String explanation;

    private Question(Integer index, String label, List<Answer> answers, Set<Answer> correctAnswers, String explanation) {
        this.index = index;
        this.label = label;
        this.answers = answers;
        this.correctAnswers = correctAnswers;
        this.explanation = explanation;
    }

    public Integer getIndex() {
        return index;
    }

    public String getLabel() {
        return label;
    }


    public boolean isCorrectAnswer(char userAnswer) {
        Set<Character> correctLetters = getCorrectLetters();
        return correctLetters.equals(Set.of(Character.toUpperCase(userAnswer)));
    }

    private Set<Character> getCorrectLetters() {
        Set<Character> correctLetters = new HashSet<>();

        for (Answer correctAnswer : correctAnswers) {
            correctLetters.add(Character.toUpperCase(correctAnswer.getLetter()));
        }

        return correctLetters;
    }
}
