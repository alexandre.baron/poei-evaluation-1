package com.zenika.poei.evaluation.domain;

public class QuestionError {

    private final Question question;
    private final char userResponse;

    public QuestionError(Question question, char userResponse) {
        this.question = question;
        this.userResponse = userResponse;
    }

    public Question getQuestion() {
        return question;
    }

    public char getUserResponse() {
        return userResponse;
    }
}
