package com.zenika.poei.evaluation.domain;

public class Answer {

    private final char letter;
    private final String label;

    public Answer(char letter, String label) {
        this.letter = letter;
        this.label = label;
    }

    public char getLetter() {
        return letter;
    }

    public String getLabel() {
        return label;
    }
}
