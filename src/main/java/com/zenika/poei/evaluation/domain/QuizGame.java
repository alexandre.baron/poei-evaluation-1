package com.zenika.poei.evaluation.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class QuizGame {

    private Iterator<Question> remainingQuestions;
    private Question currentQuestion;
    private int score = 0;
    private final int maxScore;
    private final List<QuestionError> errors = new ArrayList<>();


    public QuizGame(List<Question> questions) {
        this.maxScore = questions.size();
        remainingQuestions = questions.iterator();

        updateCurrentQuestion();
    }

    public Question getCurrentQuestion() {
        return currentQuestion;
    }

    public void choose(char userAnswer) {
        if (currentQuestion.isCorrectAnswer(userAnswer)) {
            score++;
        } else {
            errors.add(new QuestionError(currentQuestion, userAnswer));
        }

        updateCurrentQuestion();
    }

    private void updateCurrentQuestion() {
        if (remainingQuestions.hasNext()) {
            currentQuestion = remainingQuestions.next();
        } else {
            currentQuestion = null;
        }
    }

    public boolean isFinish() {
        return currentQuestion == null;
    }

    public int getScore() {
        return score;
    }

    public int getMaxScore() {
        return maxScore;
    }

    public List<QuestionError> getErrors() {
        return errors;
    }
}
