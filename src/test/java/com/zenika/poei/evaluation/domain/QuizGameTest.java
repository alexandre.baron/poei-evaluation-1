package com.zenika.poei.evaluation.domain;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class QuizGameTest {

    Question weatherQuestion = Question.question(
            "Quel temps fait-il ?",
            List.of(
                    new Answer('A', "Beau"),
                    new Answer('B', "Mauvais")),
            Set.of(new Answer('B', "Mauvais")),
            "En Bretagne, il fait toujours mauvais.");

    Question ageQuestion = Question.question(
            "Est-tu majeur ?",
            List.of(
                    new Answer('A', "Oui"),
                    new Answer('B', "Non")),
            Set.of(new Answer('A', "Oui")),
            "Et oui, tu es grand.");

    Quiz quiz = new Quiz(List.of(
            weatherQuestion,
            ageQuestion));

    @Test
    void shouldPlayAQuizGame() {
        QuizGame quizGame = quiz.newGame();

        // Question 1 - weather
        Question question1 = quizGame.getCurrentQuestion();
        assertThat(question1.getIndex()).isEqualTo(1);
        assertThat(question1.getLabel()).isEqualTo(weatherQuestion.getLabel());

        assertThat(quizGame.isFinish()).isFalse();

        quizGame.choose('B');

        // Question 2 - age
        Question question2 = quizGame.getCurrentQuestion();
        assertThat(question2.getIndex()).isEqualTo(2);
        assertThat(question2.getLabel()).isEqualTo(ageQuestion.getLabel());

        assertThat(quizGame.isFinish()).isFalse();

        quizGame.choose('A');

        // Results
        assertThat(quizGame.isFinish()).isTrue();
        assertThat(quizGame.getScore()).isEqualTo(2);
        assertThat(quizGame.getMaxScore()).isEqualTo(2);
        assertThat(quizGame.getErrors()).hasSize(0);
    }

    @Test
    void shouldHaveOneErrorInGame() {
        QuizGame quizGame = quiz.newGame();

        quizGame.choose('A');
        quizGame.choose('A');

        assertThat(quizGame.isFinish()).isTrue();
        assertThat(quizGame.getScore()).isEqualTo(1);
        assertThat(quizGame.getMaxScore()).isEqualTo(2);

        // Check errors
        assertThat(quizGame.getErrors()).hasSize(1);
        QuestionError firstError = quizGame.getErrors().get(0);
        assertThat(firstError.getQuestion().getIndex()).isEqualTo(1);
        assertThat(firstError.getQuestion().getLabel()).isEqualTo(weatherQuestion.getLabel());
        assertThat(firstError.getUserResponse()).isEqualTo('A');
    }

    @Test
    void shouldHaveTwoErrorInGame() {
        QuizGame quizGame = quiz.newGame();

        quizGame.choose('A');
        quizGame.choose('B');

        assertThat(quizGame.isFinish()).isTrue();
        assertThat(quizGame.getScore()).isEqualTo(0);
        assertThat(quizGame.getMaxScore()).isEqualTo(2);

        // Check errors
        assertThat(quizGame.getErrors()).hasSize(2);

        QuestionError firstError = quizGame.getErrors().get(0);
        assertThat(firstError.getQuestion().getIndex()).isEqualTo(1);
        assertThat(firstError.getQuestion().getLabel()).isEqualTo(weatherQuestion.getLabel());
        assertThat(firstError.getUserResponse()).isEqualTo('A');

        QuestionError secondError = quizGame.getErrors().get(1);
        assertThat(secondError.getQuestion().getIndex()).isEqualTo(2);
        assertThat(secondError.getQuestion().getLabel()).isEqualTo(ageQuestion.getLabel());
        assertThat(secondError.getUserResponse()).isEqualTo('B');
    }
}
