package com.zenika.poei.evaluation.domain;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class QuestionTest {

    Question weatherQuestion = Question.question(
            "Quel temps fait-il ?",
            List.of(
                    new Answer('A', "Beau"),
                    new Answer('B', "Mauvais")),
            Set.of(new Answer('B', "Mauvais")),
            "En Bretagne, il fait toujours mauvais.");

    @Test
    void shouldValidateCorrectAnswer() {
        assertThat(weatherQuestion.isCorrectAnswer('B')).isTrue();
    }

    @Test
    void shouldValidateCorrectLowercaseAnswer() {
        assertThat(weatherQuestion.isCorrectAnswer('b')).isTrue();
    }

    @Test
    void shouldInvalidateIncorrectAnswer() {
        assertThat(weatherQuestion.isCorrectAnswer('A')).isFalse();
    }
}